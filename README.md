# AI template

Template for AI python projects.

- jupyter
- scikit-learn
- matplotlib - numpy - pandas

## Use

```
git clone https://codeberg.org/neuronaddict/python-ai-template.git
cd python-ai-template
python3 -m venv venv
source venv/bin/activate
pip install -r dev-requirements.txt
pip install -r requirements.txt
```

## Upgrade versions

```
pip-compile dev-requirements.in
pip-compile requirements.in
```

And commit *requirements.txt files.

## Offline Docs

To use offline doc :

### numpy

/usr/share/doc/python-numpy-doc/html/index.html

(apt install python-numpy-doc)

### pandas

/usr/share/doc/python-pandas-doc/html/index.html

(apt install python-pandas-doc)

### matplotlib

/usr/share/doc/python-matplotlib-doc/html/index.html

(apt install python-matplotlib-doc)

## other docs

### scikit-learn

https://scikit-learn.org/stable/user_guide.html

### nltk

https://www.nltk.org/

## pre-commit

```
pre-commit install
```

notebook are cleans at commit with nb-clean.
Note: ensure there is not an old pre-commit version on ~/.local
